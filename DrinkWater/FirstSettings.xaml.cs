﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DrinkWater
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FirstSettings : Page
    {
        //private string gender;
        private string climate;
        private string activity;
        private string unitWeight;
        private string unitCapacity;
        private string gender;
        private int weight = 30;

        public FirstSettings()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.unitWeight = SettingsManager.UnitWeight;
            this.unitCapacity = SettingsManager.UnitCapacity;

            rdbKg.IsChecked = true;
            rdbMl.IsChecked = true;

            this.gender = SettingsManager.Gender;
            this.weight = SettingsManager.Weight;
            this.activity = SettingsManager.Activity;
            this.climate = SettingsManager.Climate;

            if (SettingsManager.Climate != null)
                Proceed();
        }

        private void btnProceed_Click(object sender, RoutedEventArgs e)
        {
            fillEmptyFields();
            
            SettingsManager.UnitCapacity = unitCapacity;
            SettingsManager.UnitWeight = unitWeight;
            SettingsManager.Gender = gender;
            SettingsManager.Weight = weight;
            SettingsManager.Activity = activity;
            SettingsManager.Climate = climate;
            Proceed();
        }

        private void fillEmptyFields()
        {
            if (String.IsNullOrEmpty(this.climate) || String.IsNullOrEmpty(this.activity) || String.IsNullOrEmpty(this.gender))
            {
                this.climate = (cmbClimate.SelectedItem as ComboBoxItem).Content.ToString();
                this.activity = (cmbActivity.SelectedItem as ComboBoxItem).Content.ToString();
                this.gender = (cmbGender.SelectedItem as ComboBoxItem).Content.ToString();
            }
            
        }

        private void Proceed()
        {
            Frame.Navigate(typeof(Home));
        }

        private void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string value = ((sender as ComboBox).SelectedItem as ComboBoxItem).Content.ToString();
            switch ((sender as ComboBox).Name)
            {
                case "cmbGender": this.gender = value; break;
                case "cmbClimate": this.climate = value; break;
                case "cmbActivity": this.activity = value; break;
            }
        }

        private void weight_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.weight = (int)(sender as Slider).Value;
        }

        private void radioButtonWeightCheck(object sender, RoutedEventArgs e)
        {
            unitWeight = (sender as RadioButton).Tag.ToString();

            SettingsManager.UnitWeight = unitWeight;
            sldWeight.Minimum = (double)30.ToSelectedWeightUnit();
            sldWeight.Value = sldWeight.Minimum;
            this.weight = 0;
            sldWeight.Maximum = (double)200.ToSelectedWeightUnit();
            SettingsManager.UnitWeight = null;
        }
        private void radioButtonCapacityCheck(object sender, RoutedEventArgs e)
        {
            unitCapacity = (sender as RadioButton).Tag.ToString();
        }

        private void CheckRadio(params string[] units)
        {
            foreach (string unit in units)
            {
                switch (unit)
                {
                    case "ml": rdbMl.IsChecked = true; break;
                    case "oz": rdbOz.IsChecked = true; break;
                    case "kg": rdbKg.IsChecked = true; break;
                    case "lbs": rdbLbs.IsChecked = true; break;
                }

            }
        }

       
    }

}
