﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrinkWater
{
    class UserSettings
    {
        private String gender = "default";
        private String unit = "default";
        private DateTime burnDate;
        private int weight = 0;


        public UserSettings(String gender, String unit,DateTime burnDate,int weight)
        {
            this.burnDate = BurnDate;
            this.unit = Unit;
            this.weight = Weight;
            this.gender = Gender;
        }
        public string Unit
        {
            get
            {
                return unit;
            }

            set
            {
                unit = value;
            }
        }

        public string Gender
        {
            get
            {
                return gender;
            }

            set
            {
                gender = value;
            }
        }

        public DateTime BurnDate
        {
            get
            {
                return burnDate;
            }

            set
            {
                burnDate = value;
            }
        }

        public int Weight
        {
            get
            {
                return weight;
            }

            set
            {
                weight = value;
            }
        }


    }
}
