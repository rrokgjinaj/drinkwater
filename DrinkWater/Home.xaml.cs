﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using XamlAnimatedGif;
using NotificationsExtensions.ToastContent;
using Windows.UI.Notifications;
using Windows.UI.ViewManagement;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DrinkWater
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        public Home()
        {
            this.InitializeComponent();
            
            hamburgerMenuControl.ItemsSource = MenuItem.GetMainItems();
            hamburgerMenuControl.OptionsItemsSource = MenuItem.GetOptionsItems();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            _glass.Maximum = SettingsManager.DailyAmount;
            _glass.Progress = SettingsManager.GetTodayProgress();
            objective.Header = SettingsManager.DailyAmount.ToSelectedCapacityUnitWithLabel();
            remains.Header = Math.Max(0, _glass.Maximum - _glass.Progress).ToSelectedCapacityUnitWithLabel();
            consumed.Text = SettingsManager.GetTodayProgress().ToSelectedCapacityUnitWithLabel();
            double blc = SettingsManager.Balance;
            balance.Header = blc.ToString("0.###") + "%";
            if (blc < 40 && SettingsManager.isNotifyEnabledNow())
            {
                IToastImageAndText02 ciao = ToastContentFactory.CreateToastImageAndText02();
                ciao.TextHeading.Text = "DrinkWater";
                ciao.TextBodyWrap.Text = "You should drink something.";
                ToastNotificationManager.CreateToastNotifier().AddToSchedule(new ScheduledToastNotification(ciao.GetXml(), DateTime.Now.AddSeconds(1)));
            }
        }

        private void OnMenuItemClick(object sender, ItemClickEventArgs e)
        {
            var menuItem = e.ClickedItem as MenuItem;
            Frame.Navigate(menuItem.PageType);
            
        }


        private void plus_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(DrinkGlass));
        }

        private async void clear_applicationData(object sender, RoutedEventArgs e)
        {
            MessageDialog clear_message= new MessageDialog("Press Yes to Clear all your Application data");
            UICommand yes = new UICommand("Yes");
            clear_message.Commands.Add(yes);
            clear_message.Commands.Add(new UICommand("No"));

            if ((await clear_message.ShowAsync())==yes) 
            {
                SettingsManager.Clear();
                Frame.Navigate(typeof(FirstSettings));
            }
            
        }
    }


    public class MenuItem
    {
        public Symbol Icon { get; set; }
        public string Name { get; set; }
        public Type PageType { get; set; }

        public static List<MenuItem> GetMainItems()
        {
            var items = new List<MenuItem>();
            items.Add(new MenuItem() { Icon = Symbol.Home, Name = "Home", PageType = typeof(Home) });
            items.Add(new MenuItem() { Icon = Symbol.Bookmarks, Name = "History", PageType = typeof(History) });
            return items;

        }

        public static List<MenuItem> GetOptionsItems()
        {
            var items = new List<MenuItem>();
            items.Add(new MenuItem() { Icon = Symbol.Setting, Name = "Settings", PageType = typeof(Settings) });
            items.Add(new MenuItem() { Icon = Symbol.ContactInfo, Name = "Info", PageType = typeof(Info) });
            return items;
        }
          
    }
}