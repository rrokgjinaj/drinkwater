﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DrinkWater
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DrinkGlass : Page
    {
        public class MyComboBoxItem
        {
            public string Title { get; set; }
            public int PercentageofWater { get; set; }

            public MyComboBoxItem(string title, int percentage)
            {
                this.Title = title;
                this.PercentageofWater = percentage;
            }

            public override string ToString()
            {
                return Title;
            }
        }

        private ObservableCollection<MyComboBoxItem> ComboBoxOptions = new ObservableCollection<MyComboBoxItem>
        {
            new MyComboBoxItem("Water",100),
            new MyComboBoxItem("Fruit Juice",85),
            new MyComboBoxItem("Beer",95)
        };
        public DrinkGlass()
        {
            this.InitializeComponent();

            SelectedComboBoxOption = ComboBoxOptions[0];
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //servono qui per far scattare i valueChanged senza problemi
            sli0.Value = sli0.Maximum;
            sli1.Value = sli1.Maximum;
            sli2.Value = sli2.Maximum;
        }

        public MyComboBoxItem SelectedComboBoxOption
        {
            get; set;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            listView.ItemClick -= listView_ItemClick;

            switch ((sender as Button).Name)
            {
                case "settings0":
                    sli0.Visibility = Visibility.Visible;
                    ComboBox0.Visibility = Visibility.Visible;
                    drinkButton0.Visibility = Visibility.Visible;

                    break;
                case "settings1":
                    sli1.Visibility = Visibility.Visible;
                    ComboBox1.Visibility = Visibility.Visible;
                    drinkButton1.Visibility = Visibility.Visible;
                    break;
                case "settings2":
                    sli2.Visibility = Visibility.Visible;
                    ComboBox2.Visibility = Visibility.Visible;
                    drinkButton2.Visibility = Visibility.Visible;
                    break;
            }
           
        }

        private void drinButton_Click(object sender, RoutedEventArgs e)
        {
            double p;
            switch ((sender as Button).Name)
            {
                case "drinkButton0":
                    p = (ComboBox0.SelectedItem as MyComboBoxItem).PercentageofWater/100.0;
                    SettingsManager.addTodayProgress((int)(sli0.Value * p));
                    break;
                case "drinkButton1":
                    p = (ComboBox1.SelectedItem as MyComboBoxItem).PercentageofWater/100.0;
                    SettingsManager.addTodayProgress((int)(sli1.Value * p));
                    break;
                case "drinkButton2":
                    p = (ComboBox2.SelectedItem as MyComboBoxItem).PercentageofWater/100.0;
                    SettingsManager.addTodayProgress((int)(sli2.Value * p));
                    break;
            }
            Frame.Navigate(typeof(Home));
        }

        private void listView_ItemClick(object sender, ItemClickEventArgs e)
        {
            SettingsManager.addTodayProgress(Int32.Parse((e.ClickedItem as Grid).Tag.ToString()));
            Frame.Navigate(typeof(Home));
        }

        private void sli_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (glass0 != null && bik0 != null && sli0.Equals(sender))
            {
                glass0.Progress = (int)(sli0.Maximum - sli0.Value);
                bik0.Text = ((int)sli0.Value).ToSelectedCapacityUnitWithLabel();
            }
            if (glass1 != null && bik1 != null && sli1.Equals(sender))
            {
                glass1.Progress = (int)(sli1.Maximum - sli1.Value);
                bik1.Text = ((int)sli1.Value).ToSelectedCapacityUnitWithLabel();
            }
            if (glass2 != null && bik2 != null && sli2.Equals(sender))
            {
                glass2.Progress = (int)(sli2.Maximum - sli2.Value);
                bik2.Text = ((int)sli2.Value).ToSelectedCapacityUnitWithLabel();
            }
        }

    }
    public class ComboBoxItemConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value as DrinkGlass.MyComboBoxItem;
        }
    }
}