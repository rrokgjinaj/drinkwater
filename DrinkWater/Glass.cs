﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using XamlAnimatedGif;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace DrinkWater
{
    public sealed class Glass : Grid
    {
        Image _gifImage;
        Border _back;
        Image _bicchiere;
        public Glass()
        {
            this.Loaded += Glass_Loaded;

            _gifImage = new Image();
            _bicchiere = new Image();

            AnimationBehavior.SetSourceUri(_gifImage, new Uri("ms-appx:///MyImages/img.gif"));
            AnimationBehavior.SetRepeatBehavior(_gifImage, new RepeatBehavior(TimeSpan.FromSeconds(2)));
            AnimationBehavior.SetRepeatBehavior(_gifImage, RepeatBehavior.Forever);
            _gifImage.Stretch = Stretch.Fill;
            _gifImage.VerticalAlignment = VerticalAlignment.Top;

            _back = new Border();
            _back.VerticalAlignment = VerticalAlignment.Top;
            
            _back.Background = new SolidColorBrush(Color.FromArgb(255, 72, 177, 229));

            _bicchiere.Source = new BitmapImage(new Uri("ms-appx:///MyImages/bicchiere.png"));
            _bicchiere.Stretch = AnalyticsInfo.VersionInfo.DeviceFamily.Equals("Windows.Desktop") && 
                UIViewSettings.GetForCurrentView().UserInteractionMode == UserInteractionMode.Mouse ? 
                Stretch.Uniform : Stretch.Fill;

            this.Children.Add(_back);
            this.Children.Add(_gifImage);
            this.Children.Add(_bicchiere);
        }

        private void Glass_Loaded(object sender, RoutedEventArgs e)
        {
            _bicchiere.SizeChanged += bicchiere_SizeChanged;
            bicchiere_SizeChanged(_bicchiere, null);
        }

        public int Progress
        {
            get { return (int)GetValue(ProgessProperty); }
            set { SetValue(ProgessProperty, value); }
        }
        public int Maximum
        {
            get { return (int)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }
        public double gifSize
        {
            get { return (double)GetValue(GifSizeProperty); }
            set { SetValue(GifSizeProperty, value); }
        }

        public static readonly DependencyProperty ProgessProperty =
            DependencyProperty.Register("Progess", typeof(int), typeof(Glass), new PropertyMetadata(0, new PropertyChangedCallback(OnProgressChanged)));
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(int), typeof(Glass), new PropertyMetadata(100, new PropertyChangedCallback(OnMaximumChanged)));
        public static readonly DependencyProperty GifSizeProperty =
            DependencyProperty.Register("GifSize", typeof(int), typeof(Glass), new PropertyMetadata(0.3, new PropertyChangedCallback(OnGifSizeChanged)));

        private static void OnProgressChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Glass).setMargin((int)e.NewValue);
        }
        private static void OnMaximumChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Glass).setMargin((d as Glass).Progress);
        }
        private static void OnGifSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((double)e.NewValue > 1)
            {
                (d as Glass).gifSize = 1;
                return;
            }
            (d as Glass).bicchiere_SizeChanged(null, null);
        }

        private void bicchiere_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _gifImage.Width = _bicchiere.ActualWidth;
            _back.Width = _gifImage.Width;
            _gifImage.Height = _bicchiere.ActualHeight * gifSize;
            setMargin(Progress);
        }

        public void setMargin(int topMargin)
        {
            _gifImage.Margin = new Thickness(0, topMargin/(double)Maximum * _bicchiere.ActualHeight /*- _gifImage.Height*/, 0, 0);

           
            _back.Margin = new Thickness(0,_gifImage.Margin.Top+_gifImage.Height,0,0);
            _back.Height = Math.Max(0, _bicchiere.ActualHeight - _back.Margin.Top);
        }
    }
}
