﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace DrinkWater
{
    static class SettingsManager
    {
        private const int DAYS_TO_SAVE = 7;

        private const string SETTING_UNITW = "unitWeight";
        private const string SETTING_UNITC = "unitCapacity";
        private const string SETTING_GENDER = "gender";
        private const string SETTING_WEIGHT = "weight";
        private const string SETTING_DATE = "date";
        private const string SETTING_REMINDERS = "reminders";
        private const string SETTING_HISTORY = "history";
        private const string SETTING_BALANCE = "balance";

        private const string SETTINGS_ACTIVITY = "activity";
        private const string SETTINGS_CLIMATE = "climate";


        private static Windows.Storage.ApplicationDataContainer LocalSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
    
        public static void Clear()
        {
            LocalSettings.Values.Clear();
        }


        public static int DailyAmount
        {
            get
            {
                //i dati ho provato a rimediarli da un sito che calcolava senza dire come!
                //se hai di meglio agisci pure
                double ActivityFactor = 0;
                if (Activity == "Sedentary") ActivityFactor = 1;
                if (Activity == "Light Activity") ActivityFactor = 1.2;
                if (Activity == "Moderately Active") ActivityFactor = 1.5;
                if (Activity == "Very Active") ActivityFactor = 2.1;
                double ClimateFactor = 0;
                if (Climate == "Cold") ClimateFactor = 0.9;
                if (Climate == "Temperate") ClimateFactor = 1;
                if (Climate == "Tropical") ClimateFactor = 1.2;
                double GenderFactor = 0;
                if (Gender == "Female") GenderFactor = 1;
                if (Gender == "Male") GenderFactor = 1.1;
                return (int)((18 + (Weight / 45)) * ActivityFactor * ClimateFactor * GenderFactor * 100);
            }
        }
        
        public static String UnitWeight
        {
            get
            {
                object o = LocalSettings.Values[SETTING_UNITW];
                return o==null ? null : o.ToString();
            }
            set { LocalSettings.Values[SETTING_UNITW] = value; }
        }
        public static String UnitCapacity
        {
            get
            {
                object o = LocalSettings.Values[SETTING_UNITC];
                return o == null ? null : o.ToString();
            }
            set { LocalSettings.Values[SETTING_UNITC] = value; }
        }
        public static string Gender
        {
            get
            {
                object o = LocalSettings.Values[SETTING_GENDER];
                return o == null ? null : o.ToString();
            }
            set { LocalSettings.Values[SETTING_GENDER] = value; }
        }

        public static string Climate
        {
            get
            {
                object o = LocalSettings.Values[SETTINGS_CLIMATE];
                return o == null ? null : o.ToString();
            }
            set { LocalSettings.Values[SETTINGS_CLIMATE] = value; }
        }

        private const decimal ozFactor = 0.0338140225589M;
        public static int GetTodayProgress()
        {
            var h = History;
            if (h == null || h.Last().Date != DateTime.Today)
            {
                addTodayProgress(0);
                return 0;
            }
            else
            {
                return h.Last().Progress;
            }
        }
        public static decimal ToSelectedCapacityUnit(this int ml)
        {
            return ml * (UnitCapacity == "oz" ? ozFactor : 1);
        }
        public static string ToSelectedCapacityUnitWithLabel(this int ml)
        {
            return ToSelectedCapacityUnit(ml).ToString("0.###") + " " + UnitCapacity;
        }

        public static string Activity
        {
            get
            {
                object o = LocalSettings.Values[SETTINGS_ACTIVITY];
                return o == null ? null : o.ToString();
            }
            set { LocalSettings.Values[SETTINGS_ACTIVITY] = value; }
        }

        private const decimal lbsFactor = 2.20462262185M;
        public static int Weight
        {
            get
            {
                object o = LocalSettings.Values[SETTING_WEIGHT];
                return o == null ? 0 : Int32.Parse(o.ToString());
            }
            set { LocalSettings.Values[SETTING_WEIGHT] = (int)(value / (UnitWeight == "kg" ? 1 : lbsFactor)); }
        }

        public static decimal ToSelectedWeightUnit(this int kg)
        {
            return kg * (UnitWeight == "lbs" ? lbsFactor : 1);
        }
        public static string ToSelectedWeightUnitWithLabel(this int kg)
        {
            return ToSelectedWeightUnit(kg).ToString("0.###") + " " + UnitWeight;
        }

        public static ObservableCollection<SettingsItem> reminders
        {
            get
            {
                object o = LocalSettings.Values[SETTING_REMINDERS];
                if (o == null) return null;
                SerializableSettingItem[] s = JsonConvert.DeserializeObject<SerializableSettingItem[]>(o.ToString());
                return new ObservableCollection<SettingsItem>(s.Select(ssi => ssi.toSettingItem()));
            }
            set
            {
                SerializableSettingItem[] s = value.Select(si => new SerializableSettingItem(si)).ToArray();
                LocalSettings.Values[SETTING_REMINDERS] = JsonConvert.SerializeObject(s);
            }
        }

        public static bool isNotifyEnabledNow()
        {
            var r = reminders;
            if (r == null) return false;
            DateTime now = DateTime.Now;
            foreach (SettingsItem item in r)
            {
                if (item.isEnabledOn(now)) return true;
            }
            return false;
        }

        public static ObservableCollection<HistoryItem> History
        {
            get
            {
                object o = LocalSettings.Values[SETTING_HISTORY];
                if (o == null) return null;
                return new ObservableCollection<HistoryItem>(JsonConvert.DeserializeObject<HistoryItem[]>(o.ToString()));
            }
            set
            {
                LocalSettings.Values[SETTING_HISTORY] = JsonConvert.SerializeObject(value.ToArray());
            }
        }
        public static void addTodayProgress(int todayProgress)
        {
            var tmp = History;
            ObservableCollection<HistoryItem> history = tmp == null ? new ObservableCollection<HistoryItem>() : History;
            HistoryItem today = history.FirstOrDefault(hi => hi.Date.Equals(DateTime.Today));
            if (today == null)
            {
                history.Add(new HistoryItem() { Date = DateTime.Today, Progress = todayProgress });
                if (history.Count > DAYS_TO_SAVE) history.RemoveAt(0);
                History = history;
            }
            else
            {
                history.Last().Progress += todayProgress;
                History = history;      
            }

            Balance += Math.Min(100, todayProgress / 5);
            lastDrink = DateTime.Now;
        }

        private static DateTime lastDrink
        {
            get
            {
                return DateTime.Parse(LocalSettings.Values["LastDrink"].ToString());
            }
            set
            {
                LocalSettings.Values["LastDrink"] = value.ToString();
            }
        }
        public static double Balance
        {
            get
            {
                if (GetTodayProgress() == 0)
                {
                    LocalSettings.Values[SETTING_BALANCE] = 0;
                    lastDrink = DateTime.Now;
                    return 0;
                }
                object o = LocalSettings.Values[SETTING_BALANCE];
                double b = o == null ? 0 : Double.Parse(o.ToString());
                if (lastDrink != null && b > 0)
                {
                    TimeSpan t = DateTime.Now - lastDrink;
                    double value = b - t.Hours * 100.0 / 6.0 - t.Minutes * 100.0 / 360.0;
                    LocalSettings.Values[SETTING_BALANCE] = value;
                    if (value != b) lastDrink = DateTime.Now;
                    return value;
                }
                return b;
            }
            private set { LocalSettings.Values[SETTING_BALANCE] = Math.Max(0, Math.Min(100, value)); }
        }

        public static double HistoryAverage
        {
            get
            {
                var tmp = History;
                return tmp==null ? 0 : tmp.Average(ht => ht.Progress);
            }
        }
        
        private class SerializableSettingItem
        {
            public bool? isEnabled;
            public string hours;
            public string days;

            public SerializableSettingItem(SettingsItem item)
            {
                if (item != null)
                {
                    this.isEnabled = item.IsEnabled;
                    this.hours = item.Hours;
                    this.days = item.Days;
                }
            }

            public SettingsItem toSettingItem()
            {
                String[] times = this.hours.Replace(" ", "").Split('-');
                return new SettingsItem(isEnabled, TimeSpan.Parse(times[0]), TimeSpan.Parse(times[1]), this.days.Replace(" ", "").Split(','));
            }
        }
    }
}
