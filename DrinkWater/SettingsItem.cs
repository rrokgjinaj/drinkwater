﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace DrinkWater
{
    public class SettingsItem : INotifyPropertyChanged
    {
        public static SolidColorBrush enabledColor = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 255, 0));
        public static SolidColorBrush disabledColor = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 0, 0));
        public static string enableCaption = "Enable";
        public static string disableCaption = "Disable";
        public static string enabledImagePath = "MyImages/checkmark-24-512.png";
        public static string disabledImagePath = "MyImages/cross-24.png";

        private TimeSpan StartingHour;
        private TimeSpan EndingHour;
        private List<String> ActiveDays;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private bool? isEnabled;
        public bool? IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        public string Hours
        {
            get { return StartingHour.ToString() + " - " + EndingHour.ToString(); }
        }
        public string Days
        {
            get
            {
                String tmp = "";
                ActiveDays.ForEach(s => tmp += s + ", ");
                return tmp.Remove(tmp.Length - 2);
            }
        }
        public SolidColorBrush Color
        {
            get { return (bool)IsEnabled ? enabledColor : disabledColor; }
        }
        public SolidColorBrush OtherColor
        {
            get { return (bool)IsEnabled ? disabledColor : enabledColor; }
        }

        public void toggleEnabled()
        {
            IsEnabled = !IsEnabled;
        }

        public SettingsItem(TimeSpan startingTime, TimeSpan endingTime, params String[] days) : this(true, startingTime, endingTime, days)
        {
        }
        public SettingsItem(bool? enabled, TimeSpan startingTime, TimeSpan endingTime, params String[] days)
        {
            this.StartingHour = startingTime;
            this.EndingHour = endingTime;
            this.ActiveDays = days.ToList();
            this.IsEnabled = enabled;
        }

        public bool isEnabledOn(DateTime dt)
        {
            if (isEnabled == false) return false;
            if (!Days.Contains(dt.DayOfWeek.ToString())) return false;
            TimeSpan now = new TimeSpan(dt.Hour, dt.Minute, dt.Second);
            if (StartingHour > now || EndingHour < now) return false;
            return true;
        }
    }

    public class isEnabledConverter : Windows.UI.Xaml.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (targetType == typeof(Visibility))
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            if (targetType == typeof(Brush) && "other".Equals(parameter))
                return (bool)value ? SettingsItem.enabledColor : SettingsItem.disabledColor;
            if (targetType==typeof(Brush))
                return (bool)value ? SettingsItem.disabledColor : SettingsItem.enabledColor;
            if (targetType == typeof(ImageSource))
                return new BitmapImage(new Uri("ms-appx:///" + ((bool)value ? SettingsItem.enabledImagePath : SettingsItem.disabledImagePath)));
            if (targetType == typeof(String))
                return (bool)value ? SettingsItem.disableCaption : SettingsItem.enableCaption;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}