﻿using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=234238

namespace DrinkWater
{
    public sealed partial class Settings : Page
    {
        private ObservableCollection<SettingsItem> _items;
        ObservableCollection<SettingsItem> tmp;

        public Settings()
        {
            this.InitializeComponent();

            tmp = SettingsManager.reminders;
            if (tmp == null)
            {
                tmp = new ObservableCollection<SettingsItem>();
            }
            _items = tmp;
        }

        private async void Add_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog d = new ContentDialog();
            StackPanel panel = new StackPanel();

            TimePicker startingTime = new TimePicker();
            startingTime.Time = new TimeSpan(0, 0, 0);
            startingTime.ClockIdentifier = "24HourClock";
            startingTime.Margin = new Thickness(10);
            startingTime.Header = "Enable reminders from";
            panel.Children.Add(startingTime);
            TimePicker endingTime = new TimePicker();
            endingTime.Time = new TimeSpan(23, 59, 0);
            endingTime.ClockIdentifier = "24HourClock";
            endingTime.Margin = new Thickness(10);
            endingTime.Header = "Disable reminders at";
            panel.Children.Add(endingTime);

            List<CheckBox> chbDays = new List<CheckBox>
            {
                new CheckBox { Content = "Sunday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Monday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Tuesday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Wednesday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Thursday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Friday", IsChecked=true, Margin=new Thickness(5) },
                new CheckBox { Content = "Saturday", IsChecked=true, Margin=new Thickness(5) }
            };
            chbDays.ForEach(chb => panel.Children.Add(chb));

            startingTime.TimeChanged += delegate {
                if (startingTime.Time > endingTime.Time) startingTime.Time = endingTime.Time;
            };
            endingTime.TimeChanged += delegate {
                if (endingTime.Time < startingTime.Time) endingTime.Time = startingTime.Time;
            };

            d.Content = panel;

            d.PrimaryButtonText = "done";
            d.IsPrimaryButtonEnabled = true;

            d.SecondaryButtonText = "cancel";
            d.IsSecondaryButtonEnabled = true;

            ContentDialogResult result = await d.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                string[] days = chbDays.Where(chb => chb.IsChecked.GetValueOrDefault(false)).Select(chb => chb.Content.ToString()).ToArray();
                tmp.Add(new SettingsItem(startingTime.Time, endingTime.Time, days));
            }

        }
        private void Done_Click(object sender, RoutedEventArgs e)
        {
            SettingsManager.reminders = tmp;
            Frame.Navigate(typeof(Home));
        }

        private void SlidableListItem_RightCommandRequested(object sender, EventArgs e)
        {
            tmp.Remove((sender as SlidableListItem).Tag as SettingsItem);
        }

        private void SlidableListItem_LeftCommandRequested(object sender, EventArgs e)
        {
            ((sender as SlidableListItem).Tag as SettingsItem).toggleEnabled();
        }
    }
}
